import mods.modularmachinery.RecipeBuilder;

var cm_tiny_machine = RecipeBuilder.newBuilder("mcz_cm_tiny_machine", "mcz_compactmachine", 1000);
cm_tiny_machine.addEnergyPerTickInput(50);
cm_tiny_machine.addItemInput(<compactmachines3:wallbreakable> * 26);
cm_tiny_machine.addItemInput(<minecraft:ender_pearl>);
cm_tiny_machine.addItemOutput(<compactmachines3:machine:0>);
cm_tiny_machine.build();

var cm_small_machine = RecipeBuilder.newBuilder("mcz_cm_small_machine", "mcz_compactmachine", 1000);
cm_small_machine.addEnergyPerTickInput(100);
cm_small_machine.addItemInput(<compactmachines3:wallbreakable> * 26);
cm_small_machine.addItemInput(<minecraft:iron_block>);
cm_small_machine.addItemInput(<minecraft:ender_pearl>);
cm_small_machine.addItemOutput(<compactmachines3:machine:1>);
cm_small_machine.build();

var cm_normal_machine = RecipeBuilder.newBuilder("mcz_cm_normal_machine", "mcz_compactmachine", 1000);
cm_normal_machine.addEnergyPerTickInput(200);
cm_normal_machine.addItemInput(<compactmachines3:wallbreakable> * 26);
cm_normal_machine.addItemInput(<minecraft:gold_block>);
cm_normal_machine.addItemInput(<minecraft:ender_pearl>);
cm_normal_machine.addItemOutput(<compactmachines3:machine:2>);
cm_normal_machine.build();

var cm_large_machine = RecipeBuilder.newBuilder("mcz_cm_large_machine", "mcz_compactmachine", 1000);
cm_large_machine.addEnergyPerTickInput(300);
cm_large_machine.addItemInput(<compactmachines3:wallbreakable> * 98);
cm_large_machine.addItemInput(<minecraft:ender_pearl>);
cm_large_machine.addItemOutput(<compactmachines3:machine:3>);
cm_large_machine.build();

var cm_giant_machine = RecipeBuilder.newBuilder("mcz_cm_giant_machine", "mcz_compactmachine", 1000);
cm_giant_machine.addEnergyPerTickInput(400);
cm_giant_machine.addItemInput(<compactmachines3:wallbreakable> * 98);
cm_giant_machine.addItemInput(<minecraft:diamond_block>);
cm_giant_machine.addItemInput(<minecraft:ender_pearl>);
cm_giant_machine.addItemOutput(<compactmachines3:machine:4>);
cm_giant_machine.build();

var cm_max_machine = RecipeBuilder.newBuilder("mcz_cm_max_machine", "mcz_compactmachine", 1000);
cm_max_machine.addEnergyPerTickInput(500);
cm_max_machine.addItemInput(<compactmachines3:wallbreakable> * 98);
cm_max_machine.addItemInput(<minecraft:emerald_block>);
cm_max_machine.addItemInput(<minecraft:ender_pearl>);
cm_max_machine.addItemOutput(<compactmachines3:machine:5>);
cm_max_machine.build();

var cm_tunnel = RecipeBuilder.newBuilder("mcz_cm_tunnel", "mcz_compactmachine", 1000);
cm_tunnel.addEnergyPerTickInput(120);
cm_tunnel.addItemInput(<minecraft:redstone> * 8);
cm_tunnel.addItemInput(<minecraft:hopper>);
cm_tunnel.addItemInput(<compactmachines3:wallbreakable>);
cm_tunnel.addItemInput(<minecraft:ender_pearl>);
cm_tunnel.addItemOutput(<compactmachines3:tunneltool>);
cm_tunnel.build();

var cm_rs_tunnel = RecipeBuilder.newBuilder("mcz_cm_rs_tunnel", "mcz_compactmachine", 1000);
cm_rs_tunnel.addEnergyPerTickInput(120);
cm_rs_tunnel.addItemInput(<minecraft:redstone> * 8);
cm_rs_tunnel.addItemInput(<minecraft:redstone_block>);
cm_rs_tunnel.addItemInput(<compactmachines3:wallbreakable>);
cm_rs_tunnel.addItemInput(<minecraft:ender_pearl>);
cm_rs_tunnel.addItemOutput(<compactmachines3:redstonetunneltool>);
cm_rs_tunnel.build();
